package xyz.mumu233.cqprobot.entity.api;

import lombok.Data;

/**
 * @Author mumu
 * @create 2019/6/26 18:09
 */
@Data
public class FeiFeiResult {

    private String result;

    private String content;
}
