package xyz.mumu233.cqprobot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class CqprobotApplication {


    public static void main(String[] args) {
        SpringApplication.run(CqprobotApplication.class, args);
        StartBoot.start();
    }
}
