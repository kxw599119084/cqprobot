package xyz.mumu233.cqprobot.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 时间日期获取工具类
 * @author mumu
 * @date 2019/8/6 10:25
 */
public class DateUtils {

    private static String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
    public static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat SDFT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 获取当前年份 例如：2019
     * @return 年份
     */
    public static String getYear(){
        Calendar cal = Calendar.getInstance();
        return String.valueOf(cal.get(Calendar.YEAR));
    }

    /**
     * 获取当前月份 例如：8
     * @return
     */
    public static String getMonth() {
        Calendar cal = Calendar.getInstance();
        return String.valueOf(cal.get(Calendar.MONTH));
    }

    /**
     * 按照yyyy-MM-dd'T'HH:mm:ss.SSS Z的格式，字符串转日期
     * @param date
     * @return
     */
    public static Date str3Date(String date) {
        if (date != null && !"".equals(date)) {
            date = date.replace("GMT", "").replaceAll("\\(.*\\)", "");
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss z", Locale.ENGLISH);
            try {
                return sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return new Date();
        } else {
            return null;
        }
    }
    /**
     * 获取当前日期 例如：6
     * @return
     */
    public static String getDay() {
        Calendar cal = Calendar.getInstance();
        return String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * 获取当前日期 yyyy-mm-dd
     * @return
     */
    public static String getToDayByYMD(){
        return SDF.format(new Date());
    }
    /**
     * 获取当前日期 yyyy-mm-dd hh:mm:ss
     * @return
     */
    public static String getToDayByYMDHMS(){
        return SDFT.format(new Date());
    }

    /**
     * 获取是今年的第几周
     * @return
     */
    public static int getWeekOfYear(){
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * 判断同当前时间对比的前后
     * @param dateString 时间字符串 yyyy-mm-dd hh:mm:ss
     * @return 0日期相等，大于0表示在当前时间之后，小于0在当前时间之前
     */
    public static int beforeDate(String dateString){
        Date oldDate = null;
        Date nowDate = new Date();
        try {
            oldDate = SDFT.parse(dateString);
            nowDate = SDFT.parse(SDFT.format(nowDate));
        }catch (ParseException e){
            e.printStackTrace();
        }
        return oldDate.compareTo(nowDate);
    }

    /**
     * 获取今天是星期几
     * @return 字符串的星期几
     */
    public static String getWeek(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if(w < 0){
            w = 0;
        }
        return weekDays[w];
    }

    /**
     * 获取两个时间之差
     * @param oldTime 过去时间
     * @param newTime 现在时间
     * @return 单位 s
     */
    public static Long diffTime(String oldTime,String newTime){
        Date oldDate = null;
        Date newDate = null;
        try {
            oldDate = SDFT.parse(oldTime);
            newDate = SDFT.parse(newTime);
        }catch (Exception e){
            e.printStackTrace();
        }
        Long rult = newDate.getTime() - oldDate.getTime();
        return rult/1000;
    }

    /**
     * 给当前时间增加时分秒
     * @param baseDate 基础时间
     * @param hour 小时
     * @param minute 分钟
     * @param second
     * @return
     */
    public static Date add(Date baseDate, int hour,int minute, int second){
        Calendar cal = Calendar.getInstance();
        cal.setTime(baseDate);
        cal.add(Calendar.HOUR,hour);
        cal.add(Calendar.MINUTE, minute);
        cal.add(Calendar.SECOND, second);

        Date result = cal.getTime();
        return result;
    }
    /**
     * 给当前时间设置时分秒
     * @param baseDate 基础时间
     * @param hour 小时
     * @param minute 分钟
     * @param second
     * @return
     */
    public static Date set(Date baseDate, int hour,int minute, int second){
        Calendar cal = Calendar.getInstance();
        cal.setTime(baseDate);
        cal.set(Calendar.HOUR,hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);

        Date result = cal.getTime();
        return result;
    }

    public static boolean isLatestWeek(Date addtime) {
        //得到日历
        Calendar calendar = Calendar.getInstance();
        //把当前时间赋给日历
        calendar.setTime(new Date());
        //设置为7天前
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        //得到7天前的时间
        Date before7days = calendar.getTime();
        if (before7days.getTime() < addtime.getTime()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 通过时间秒毫秒数判断两个时间的间隔
     * @param date1
     * @param date2
     * @return 天数
     */
    public static int differentDaysByMillisecond(Date date1,Date date2)
    {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int day1= cal1.get(Calendar.DAY_OF_YEAR);
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);

        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);
        //同一年
        if(year1 != year2)
        {
            int timeDistance = 0 ;
            for(int i = year1 ; i < year2 ; i ++)
            {
                //闰年
                if(i%4==0 && i%100!=0 || i%400==0)
                {
                    timeDistance += 366;
                }
                else    //不是闰年
                {
                    timeDistance += 365;
                }
            }

            return timeDistance + (day2-day1) ;
        }
        else    //不同年
        {
            return day2-day1;
        }
    }
}
