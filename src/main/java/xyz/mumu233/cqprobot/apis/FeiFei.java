package xyz.mumu233.cqprobot.apis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import xyz.mumu233.cqprobot.entity.api.FeiFeiResult;

import java.io.IOException;

/**
 * @Author mumu
 * @create 2019/6/26 18:05
 */
public class FeiFei {

    public static FeiFeiResult getReply(String msg) {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("http://api.qingyunke.com/api.php?key=free&appid=0&msg=" + msg)
                .get()
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "3925faeb-fc90-7c15-deb0-44c9c74c249c")
                .build();

        try {
            Response response = client.newCall(request).execute();
            JSONObject jsonObject = JSONObject.parseObject(response.body().string());
            FeiFeiResult feifeiIResult = JSON.toJavaObject(jsonObject, FeiFeiResult.class);
            return feifeiIResult;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
