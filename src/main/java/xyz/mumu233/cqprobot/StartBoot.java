package xyz.mumu233.cqprobot;

import com.forte.qqrobot.beans.messages.result.LoginQQInfo;
import com.forte.qqrobot.component.forhttpapi.HttpApp;
import com.forte.qqrobot.component.forhttpapi.HttpApplication;
import com.forte.qqrobot.component.forhttpapi.HttpConfiguration;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import xyz.mumu233.cqprobot.utils.DateUtils;

import java.util.Date;

/**
 * @author mumu
 * @date 2019/11/01 下午3:57
 **/
@Component
@Slf4j
public class StartBoot implements HttpApp {

    private static final Integer JAVA_PORT = 15514;
    private static final String SERVLET_PATH = "/coolq";
    private static final String LOCAL_IP = "172.17.0.1";
    private static final Integer SERVLET_PORT = 8877;
    private static final String LOCAL_QQ = "2901427129";

    @Override
    public void before(HttpConfiguration configuration) {
        configuration.setJavaPort(JAVA_PORT);
        configuration.setServerPath(SERVLET_PATH);
        configuration.setServerPort(SERVLET_PORT);
        configuration.setIp(LOCAL_IP);
        configuration.setLocalQQCode(LOCAL_QQ);
    }

    @Override
    public void after(CQCodeUtil cqCodeUtil, MsgSender sender) {
        LoginQQInfo loginInfo =  sender.getLoginInfo();
        log.info("启动完毕: " + loginInfo.getName() + " 欢迎回来!");
        sender.SENDER.sendGroupMsg("569326137", "程序加载完毕" + DateUtils.getToDayByYMDHMS());
        log.info("已发送启动信息至 569326137");
    }

    public static void start() {
        new HttpApplication().run(new StartBoot());
    }
}
