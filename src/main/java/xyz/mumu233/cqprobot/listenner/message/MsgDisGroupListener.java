package xyz.mumu233.cqprobot.listenner.message;

import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;

/**
 * @author mumu
 * @date 2019/11/02 下午2:52
 * 讨论组消息
 **/
@Listen(MsgGetTypes.discussMsg)
public class MsgDisGroupListener{

}
