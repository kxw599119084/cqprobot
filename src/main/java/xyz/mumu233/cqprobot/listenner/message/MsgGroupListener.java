package xyz.mumu233.cqprobot.listenner.message;

import com.forte.qqrobot.anno.Filter;
import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.beans.messages.msgget.GroupMsg;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author mumu
 * @date 2019/11/02 下午2:52
 * 群消息
 **/
@Listen(MsgGetTypes.groupMsg)
@Slf4j
public class MsgGroupListener{

    @Filter
    public void allMessage1(GroupMsg msg, MsgGetTypes msgGetTypes, boolean at, MsgSender sender, CQCodeUtil cqCodeUtil) {
        log.info(msg.getMsg());
        sender.SENDER.sendGroupMsg(msg.getGroup(),msg.getMsg());
    }
}
