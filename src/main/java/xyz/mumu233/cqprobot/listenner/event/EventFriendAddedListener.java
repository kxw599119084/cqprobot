package xyz.mumu233.cqprobot.listenner.event;

import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;

/**
 * @author mumu
 * @date 2019/11/02 下午2:49
 *
 **/
@Listen(MsgGetTypes.friendAdd)
public class EventFriendAddedListener {
}
